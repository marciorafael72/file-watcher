package br.com.marcio.filewatcher;

import br.com.marcio.filewatcher.configuration.ApplicationConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FileWatcherApplication {

  public static void main(String[] args) {
    SpringApplication.run(FileWatcherApplication.class, args);
    ApplicationConfiguration.init();
  }
}

package br.com.marcio.filewatcher.builder;

import java.io.Serializable;
import java.util.regex.Matcher;

public interface BasicBuilder<OUTPUT extends Serializable> {

  OUTPUT buildOutput(Matcher matcher);
}

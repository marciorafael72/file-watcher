package br.com.marcio.filewatcher.builder;

import br.com.marcio.filewatcher.output.ClientOutput;
import java.util.regex.Matcher;
import org.springframework.stereotype.Component;

@Component
public class ClientBuilder implements BasicBuilder<ClientOutput> {

  public ClientOutput buildOutput(Matcher matcher) {
    return new ClientOutput(matcher.group(3), matcher.group(2), matcher.group(4));
  }
}

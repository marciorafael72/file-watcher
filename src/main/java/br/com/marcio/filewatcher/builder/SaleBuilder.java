package br.com.marcio.filewatcher.builder;

import br.com.marcio.filewatcher.output.SaleOutput;
import br.com.marcio.filewatcher.converter.SaleProductConverter;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import org.springframework.stereotype.Component;

@Component
public class SaleBuilder implements BasicBuilder<SaleOutput> {

  private SaleProductConverter saleProductConverter;

  public SaleBuilder(SaleProductConverter saleProductConverter) {
    this.saleProductConverter = saleProductConverter;
  }

  public SaleOutput buildOutput(Matcher matcher) {

    String productsLine = matcher.group(3).replaceAll("^\\[|\\]$", "");

    List<String> productsListLine = Arrays.asList(productsLine.split(","));

    return new SaleOutput(
        Integer.parseInt(matcher.group(2)),
        saleProductConverter.toProductList(productsListLine),
        matcher.group(4));
  }
}

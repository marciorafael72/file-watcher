package br.com.marcio.filewatcher.builder;

import br.com.marcio.filewatcher.domain.SaleProduct;
import java.util.regex.Matcher;
import org.springframework.stereotype.Component;

@Component
public class SaleProductBuilder {

  public SaleProduct build(Matcher matcher) {
    return new SaleProduct(
        Integer.parseInt(matcher.group(1)),
        Integer.parseInt(matcher.group(2)),
        Double.parseDouble(matcher.group(3)));
  }
}

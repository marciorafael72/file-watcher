package br.com.marcio.filewatcher.builder;

import br.com.marcio.filewatcher.output.SellerOutput;
import java.util.regex.Matcher;
import org.springframework.stereotype.Component;

@Component
public class SellerBuilder implements BasicBuilder<SellerOutput> {

  public SellerOutput buildOutput(Matcher matcher) {
    return new SellerOutput(
        matcher.group(3), matcher.group(2), Double.parseDouble(matcher.group(4)));
  }
}

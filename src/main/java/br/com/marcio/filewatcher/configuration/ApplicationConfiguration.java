package br.com.marcio.filewatcher.configuration;

import br.com.marcio.filewatcher.service.FileService;
import br.com.marcio.filewatcher.service.WatcherService;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class ApplicationConfiguration {

  private static WatcherService watcherService;
  private static FileService fileService;
  private static Logger logger = LoggerFactory.getLogger(ApplicationConfiguration.class);

  public ApplicationConfiguration(WatcherService watcherService, FileService fileService) {
    ApplicationConfiguration.watcherService = watcherService;
    ApplicationConfiguration.fileService = fileService;
  }

  public static void init() {
    Path inputPath = Paths.get(System.getProperty("user.home") + "/data/in");
    Path outputPath = Paths.get(System.getProperty("user.home") + "/data/out/");

    try {
      ApplicationConfiguration.fileService.checkDatFilesInPath(inputPath, outputPath);
      ApplicationConfiguration.watcherService.watchDatFilesInPath(inputPath, outputPath);

    } catch (Exception e) {
      logger.error(e.getMessage());
    }
  }
}

package br.com.marcio.filewatcher.converter;

import java.io.Serializable;
import java.util.regex.Pattern;

public interface BasicConverter<OUTPUT extends Serializable> {

  OUTPUT toOutput(Pattern pattern, String value);
}

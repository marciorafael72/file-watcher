package br.com.marcio.filewatcher.converter;

import br.com.marcio.filewatcher.builder.BasicBuilder;
import br.com.marcio.filewatcher.output.ClientOutput;
import java.util.regex.Pattern;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ClientConverter extends GenericConverter<ClientOutput> {

  @Value("${default.client.regex}")
  private String regex;

  public ClientConverter(BasicBuilder<ClientOutput> builder) {
    super(builder);
  }

  public ClientOutput convert(String value) {

    Pattern pattern = Pattern.compile(regex);
    return super.toOutput(pattern, value);
  }

  public String getRegex() {
    return regex;
  }

  public void setRegex(String regex) {
    this.regex = regex;
  }
}

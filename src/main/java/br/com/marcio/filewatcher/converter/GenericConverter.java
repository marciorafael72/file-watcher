package br.com.marcio.filewatcher.converter;

import br.com.marcio.filewatcher.builder.BasicBuilder;
import br.com.marcio.filewatcher.exception.InvalidFieldException;
import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class GenericConverter<OUTPUT extends Serializable>
    implements BasicConverter<OUTPUT> {

  private BasicBuilder<OUTPUT> builder;

  public GenericConverter(BasicBuilder<OUTPUT> builder) {
    this.builder = builder;
  }

  @Override
  public OUTPUT toOutput(Pattern pattern, String value) {

    Matcher matcher = pattern.matcher(value);
    matcher.find();

    if (!matcher.matches()) {
      throw new InvalidFieldException("Não foi possível validar as informações do cliente.");
    }

    return builder.buildOutput(matcher);
  }
}

package br.com.marcio.filewatcher.converter;

import br.com.marcio.filewatcher.builder.BasicBuilder;
import br.com.marcio.filewatcher.output.SaleOutput;
import java.util.regex.Pattern;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SaleConverter extends GenericConverter<SaleOutput> {

  @Value("${default.sale.regex}")
  private String regex;

  public SaleConverter(BasicBuilder<SaleOutput> builder) {
    super(builder);
  }

  public SaleOutput convert(String value) {
    Pattern pattern = Pattern.compile(regex);
    return super.toOutput(pattern, value);
  }

  public String getRegex() {
    return regex;
  }

  public void setRegex(String regex) {
    this.regex = regex;
  }
}

package br.com.marcio.filewatcher.converter;

import br.com.marcio.filewatcher.domain.SaleProduct;
import br.com.marcio.filewatcher.exception.InvalidFieldException;
import br.com.marcio.filewatcher.builder.SaleProductBuilder;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SaleProductConverter {

  @Value("${default.sale-product.regex}")
  private String regex;

  public List<SaleProduct> toProductList(List<String> productLines) {

    Pattern pattern = Pattern.compile(regex);

    List<SaleProduct> products = new ArrayList<>();
    for (String line : productLines) {
      Matcher matcher = pattern.matcher(line);
      matcher.find();

      if (!matcher.matches()) {
        throw new InvalidFieldException("Não foi possível validar as informações do produto.");
      }

      products.add(new SaleProductBuilder().build(matcher));
    }

    return products;
  }

  public String getRegex() {
    return regex;
  }

  public void setRegex(String regex) {
    this.regex = regex;
  }
}

package br.com.marcio.filewatcher.converter;

import br.com.marcio.filewatcher.builder.BasicBuilder;
import br.com.marcio.filewatcher.output.SellerOutput;
import java.util.regex.Pattern;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SellerConverter extends GenericConverter<SellerOutput> {

  @Value("${default.seller.regex}")
  private String regex;

  public SellerConverter(BasicBuilder<SellerOutput> builder) {
    super(builder);
  }

  public SellerOutput convert(String value) {
    Pattern pattern = Pattern.compile(regex);
    return super.toOutput(pattern, value);
  }

  public String getRegex() {
    return regex;
  }

  public void setRegex(String regex) {
    this.regex = regex;
  }
}

package br.com.marcio.filewatcher.domain;

public class Client extends Person {

  private String businessArea;

  public Client(String name, String document, String businessArea) {
    super(name, document);
    this.businessArea = businessArea;
  }

  public String getBusinessArea() {
    return businessArea;
  }

  public void setBusinessArea(String businessArea) {
    this.businessArea = businessArea;
  }
}

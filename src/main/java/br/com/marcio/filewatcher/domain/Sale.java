package br.com.marcio.filewatcher.domain;

import java.io.Serializable;
import java.util.List;

public class Sale implements Serializable {

  private int id;
  private List<SaleProduct> saleProducts;
  private String sellerName;

  public Sale() {}

  public Sale(int id, List<SaleProduct> saleProducts, String sellerName) {
    this.id = id;
    this.saleProducts = saleProducts;
    this.sellerName = sellerName;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public List<SaleProduct> getSaleProducts() {
    return saleProducts;
  }

  public void setSaleProducts(List<SaleProduct> saleProducts) {
    this.saleProducts = saleProducts;
  }

  public String getSellerName() {
    return sellerName;
  }

  public void setSellerName(String sellerName) {
    this.sellerName = sellerName;
  }

  public double getTotalSalePrice() {
    return saleProducts.stream().mapToDouble(SaleProduct::getTotal).sum();
  }
}

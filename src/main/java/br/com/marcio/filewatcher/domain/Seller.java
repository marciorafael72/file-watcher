package br.com.marcio.filewatcher.domain;

public class Seller extends Person {

  private double salary;

  public Seller(String name, String document, double salary) {
    super(name, document);
    this.salary = salary;
  }

  public double getSalary() {
    return salary;
  }

  public void setSalary(double salary) {
    this.salary = salary;
  }
}

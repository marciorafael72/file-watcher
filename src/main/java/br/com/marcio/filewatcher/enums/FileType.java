package br.com.marcio.filewatcher.enums;

public enum FileType {
  DAT(".dat");

  private final String type;

  FileType(String type) {
    this.type = type;
  }

  public String getType() {
    return type;
  }
}

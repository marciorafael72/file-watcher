package br.com.marcio.filewatcher.exception;

public class InvalidFormatException extends RuntimeException {

  public InvalidFormatException(String message) {
    super(message);
  }
}

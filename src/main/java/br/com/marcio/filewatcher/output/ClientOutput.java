package br.com.marcio.filewatcher.output;

import br.com.marcio.filewatcher.domain.Client;

public class ClientOutput extends Client {

  public ClientOutput(String name, String document, String businessArea) {
    super(name, document, businessArea);
  }
}

package br.com.marcio.filewatcher.output;

import br.com.marcio.filewatcher.domain.SaleProduct;
import br.com.marcio.filewatcher.domain.Sale;
import java.util.List;

public class SaleOutput extends Sale {

  public SaleOutput(int id, List<SaleProduct> saleProducts, String sellerName) {
    super(id, saleProducts, sellerName);
  }
}

package br.com.marcio.filewatcher.output;

import br.com.marcio.filewatcher.domain.Seller;

public class SellerOutput extends Seller {

  public SellerOutput(String name, String document, double salary) {
    super(name, document, salary);
  }
}

package br.com.marcio.filewatcher.service;

import br.com.marcio.filewatcher.converter.ClientConverter;
import br.com.marcio.filewatcher.converter.SaleConverter;
import br.com.marcio.filewatcher.converter.SellerConverter;
import br.com.marcio.filewatcher.output.SaleOutput;
import br.com.marcio.filewatcher.output.SellerOutput;
import br.com.marcio.filewatcher.domain.Sale;
import br.com.marcio.filewatcher.enums.FileType;
import br.com.marcio.filewatcher.exception.InvalidFormatException;
import br.com.marcio.filewatcher.output.ClientOutput;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class FileService {

  private ClientConverter clientConverter;
  private SellerConverter sellerConverter;
  private SaleConverter saleConverter;
  private Logger logger = LoggerFactory.getLogger(FileService.class);

  public FileService(
      ClientConverter clientConverter,
      SellerConverter sellerConverter,
      SaleConverter saleConverter) {
    this.clientConverter = clientConverter;
    this.sellerConverter = sellerConverter;
    this.saleConverter = saleConverter;
  }

  public Map<String, List<?>> getValuesFromDatFile(Path filePath) throws IOException {

    String fileName = filePath.getFileName().toString();

    if (fileName.endsWith(FileType.DAT.getType())) {
      List<String> lines = Files.readAllLines(filePath);

      List<String> sellers = filterLinesById("001", lines);

      List<String> clients = filterLinesById("002", lines);

      List<String> sales = filterLinesById("003", lines);

      List<ClientOutput> clientOutputList =
          clients.stream().map(clientConverter::convert).collect(Collectors.toList());

      List<SellerOutput> sellerOutputList =
          sellers.stream().map(sellerConverter::convert).collect(Collectors.toList());

      List<Sale> saleOutputList =
          sales.stream().map(saleConverter::convert).collect(Collectors.toList());

      HashMap<String, List<?>> response = new HashMap<>();
      response.put("clients", clientOutputList);
      response.put("sellers", sellerOutputList);
      response.put("sales", saleOutputList);

      return response;
    }
    throw new InvalidFormatException("O arquivo: " + fileName + " não está no formato .dat");
  }

  private List<String> filterLinesById(String id, List<String> lines) {

    List<String> filteredLine =
        lines.stream().filter(value -> value.startsWith(id)).collect(Collectors.toList());
    lines.removeAll(filteredLine);
    return filteredLine;
  }

  public List<Path> checkDatFilesInPath(Path inputPath, Path outputPath) throws IOException {

    List<Path> createdFilesPath = new ArrayList<>();

    try (Stream<Path> paths = Files.walk(inputPath)) {
      paths
          .filter(Files::isRegularFile)
          .forEach(
              p -> {
                try {
                  String fileName = p.getFileName().toString();
                  logger.info(String.format("Analisando o conteudo do arquivo: %s", fileName));
                  createdFilesPath.add(
                      createFileFromResponseDatFile(outputPath, getValuesFromDatFile(p), fileName));
                } catch (Exception ex) {
                  logger.info(ex.getMessage());
                }
              });
    }
    return createdFilesPath;
  }

  public Path createFileFromResponseDatFile(
      Path outputPath, Map<String, List<?>> response, String datFileName) throws IOException {

    List<ClientOutput> clients = (List<ClientOutput>) response.get("clients");
    List<SellerOutput> sellers = (List<SellerOutput>) response.get("sellers");
    List<SaleOutput> sales = (List<SaleOutput>) response.get("sales");

    SaleOutput highestSale =
        sales.stream()
            .max(Comparator.comparing(Sale::getTotalSalePrice))
            .orElseThrow(NullPointerException::new);
    SaleOutput lowerSale =
        sales.stream()
            .min(Comparator.comparing(Sale::getTotalSalePrice))
            .orElseThrow(NullPointerException::new);
    int clientsSize = clients.size();
    int sellersSize = sellers.size();

    JSONObject contentFile = new JSONObject();
    contentFile.put("clientsSize", clientsSize);
    contentFile.put("sellersSize", sellersSize);
    contentFile.put("idHighestSale", highestSale.getId());
    contentFile.put("worstSellerName", lowerSale.getSellerName());

    String doneFileName =
        datFileName
            .concat("-")
            .concat(String.valueOf(System.currentTimeMillis()))
            .concat(".done.dat");
    Path outputFilePath = outputPath.resolve(doneFileName);
    Files.write(outputFilePath, contentFile.toString().getBytes());
    logger.info(
        String.format(
            "O arquivo: %s foi criado com sucesso contendo os valores extraidos do arquivo: %s",
            doneFileName, datFileName));

    return outputFilePath;
  }
}

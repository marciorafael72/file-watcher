package br.com.marcio.filewatcher.service;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class WatcherService {

  private FileService fileService;
  private Logger logger = LoggerFactory.getLogger(WatcherService.class);

  public WatcherService(FileService fileService) {
    this.fileService = fileService;
  }

  public void watchDatFilesInPath(Path inputPath, Path outputPath)
      throws IOException, InterruptedException {

    try (WatchService watchService = FileSystems.getDefault().newWatchService()) {

      inputPath.register(
          watchService, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_MODIFY);

      while (true) {
        WatchKey key = watchService.take();

        for (WatchEvent<?> watchEvent : key.pollEvents()) {
          Path datFilepath = inputPath.resolve((Path) watchEvent.context());
          String datFileName = datFilepath.getFileName().toString();

          if (logger.isDebugEnabled())
            logger.debug(String.format("Analisando o conteudo do arquivo: %s", datFileName));

          try {
            Map<String, List<?>> values = fileService.getValuesFromDatFile(datFilepath);
            fileService.createFileFromResponseDatFile(outputPath, values, datFileName);
          } catch (Exception ex) {
            logger.info(ex.getMessage());
          }
        }

        boolean valid = key.reset();
        if (!valid) {
          break;
        }
      }
    }
  }
}

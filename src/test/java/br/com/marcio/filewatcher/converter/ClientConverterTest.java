package br.com.marcio.filewatcher.converter;

import br.com.marcio.filewatcher.builder.ClientBuilder;
import br.com.marcio.filewatcher.exception.InvalidFieldException;
import br.com.marcio.filewatcher.output.ClientOutput;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest
public class ClientConverterTest {

  private ClientConverter clientConverter;

  @Before
  public void init() {
    clientConverter = new ClientConverter(new ClientBuilder());
    clientConverter.setRegex("(002)ç([0-9]{16})ç([\\s\\S]+)ç([\\s\\S]+)");
  }

  @Test
  public void testValidClientInformation() {

    String clientInformation = "002ç2345675434544345çJose da SilvaçRural";
    ClientOutput clientOutput = clientConverter.convert(clientInformation);

    Assert.assertNotNull(clientOutput);
    Assert.assertEquals("Jose da Silva", clientOutput.getName());
    Assert.assertEquals("2345675434544345", clientOutput.getDocument());
    Assert.assertEquals("Rural", clientOutput.getBusinessArea());
  }

  @Test
  public void testInvalidClientInformation() {

    boolean status = true;
    try {
      String clientInformation = "001ç234567435434544345çJose da SilvaçRural";
      clientConverter.convert(clientInformation);
    } catch (InvalidFieldException ex) {
      status = false;
    }
    Assert.assertFalse(status);
  }
}

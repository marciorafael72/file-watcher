package br.com.marcio.filewatcher.converter;

import br.com.marcio.filewatcher.builder.SaleBuilder;
import br.com.marcio.filewatcher.exception.InvalidFieldException;
import br.com.marcio.filewatcher.output.SaleOutput;
import br.com.marcio.filewatcher.output.SellerOutput;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest
public class SaleConverterTest {

  private SaleConverter saleConverter;

  @Before
  public void init() {

    SaleProductConverter saleProductConverter = new SaleProductConverter();
    saleProductConverter.setRegex("([0-9]+)-([0-9]+)-([0-9]*.?[0-9]+)");

    saleConverter = new SaleConverter(new SaleBuilder(saleProductConverter));
    saleConverter.setRegex("(003)ç([0-9]{2})ç(\\[.*\\]+)ç([\\s\\S]+)");
  }

  @Test
  public void testValidSaleInformation() {

    String saleInformation =
        "003ç01ç[1-10000000-11111000000,2-100000000-10000.50,3-100-0.10]çMarcio";
    SaleOutput saleOutput = saleConverter.convert(saleInformation);

    Assert.assertNotNull(saleOutput);
    Assert.assertEquals(01, saleOutput.getId());
    Assert.assertEquals("Marcio", saleOutput.getSellerName());
    Assert.assertEquals(saleOutput.getSaleProducts().size(), 3);
  }

  @Test
  public void testInvalidSellerInformation() {

    boolean status = true;
    try {
      String saleInformation =
          "003ç01ç[1-10000000-111110-1ç00000,2-100000000-10000.50,3-100-0.10]çMarcio";
      saleConverter.convert(saleInformation);
    } catch (InvalidFieldException ex) {
      status = false;
    }catch (NumberFormatException ex){
      status = false;
    }
    Assert.assertFalse(status);
  }
}

package br.com.marcio.filewatcher.converter;

import br.com.marcio.filewatcher.domain.SaleProduct;
import br.com.marcio.filewatcher.exception.InvalidFieldException;
import java.util.Arrays;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest
public class SaleProductConverterTest {

  private SaleProductConverter saleProductConverter;

  @Before
  public void init() {
    saleProductConverter = new SaleProductConverter();
    saleProductConverter.setRegex("([0-9]+)-([0-9]+)-([0-9]*.?[0-9]+)");
  }

  @Test
  public void testValidSaleProductInformation() {

    String products = "1-10-100000000000000,2-30-2.50,3-40-3.10";
    List<String> productsInformation = Arrays.asList(products.split(","));

    List<SaleProduct> saleProducts = saleProductConverter.toProductList(productsInformation);

    Assert.assertFalse(saleProducts.isEmpty());
    Assert.assertEquals(3, saleProducts.size());
  }

  @Test
  public void testInvalidSaleProductInformation() {

    boolean status = true;
    try {
      String products = "1-10-teste,2-30-2.50,3-40-3.10";
      List<String> productsInformation = Arrays.asList(products.split(","));
      saleProductConverter.toProductList(productsInformation);
    } catch (InvalidFieldException ex) {
      status = false;
    }
    Assert.assertFalse(status);
  }
}

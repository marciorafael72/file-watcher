package br.com.marcio.filewatcher.converter;

import br.com.marcio.filewatcher.builder.SellerBuilder;
import br.com.marcio.filewatcher.exception.InvalidFieldException;
import br.com.marcio.filewatcher.output.SellerOutput;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest
public class SellerConverterTest {

  private SellerConverter sellerConverter;

  @Before
  public void init() {
    sellerConverter = new SellerConverter(new SellerBuilder());
    sellerConverter.setRegex("(001)ç([0-9]{13})ç([\\s\\S]+)ç([0-9]*.?[0-9]+)");
  }

  @Test
  public void testValidSellerInformation() {

    String sellerInformation = "001ç1234567891234çPedroç50000";
    SellerOutput sellerOutput = sellerConverter.convert(sellerInformation);

    Assert.assertNotNull(sellerOutput);
    Assert.assertEquals("Pedro", sellerOutput.getName());
    Assert.assertEquals("1234567891234", sellerOutput.getDocument());
    Assert.assertEquals(50000, sellerOutput.getSalary(), 0.001);
  }

  @Test
  public void testInvalidSellerInformation() {

    boolean status = true;
    try {
      String sellerInformation = "001ç123456ç7891234çPedro Xç50544.000";
      sellerConverter.convert(sellerInformation);
    } catch (InvalidFieldException ex) {
      status = false;
    }
    Assert.assertFalse(status);
  }
}

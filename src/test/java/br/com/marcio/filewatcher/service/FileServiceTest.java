package br.com.marcio.filewatcher.service;

import br.com.marcio.filewatcher.builder.ClientBuilder;
import br.com.marcio.filewatcher.builder.SaleBuilder;
import br.com.marcio.filewatcher.builder.SellerBuilder;
import br.com.marcio.filewatcher.converter.ClientConverter;
import br.com.marcio.filewatcher.converter.SaleConverter;
import br.com.marcio.filewatcher.converter.SaleProductConverter;
import br.com.marcio.filewatcher.converter.SellerConverter;
import br.com.marcio.filewatcher.domain.SaleProduct;
import br.com.marcio.filewatcher.exception.InvalidFieldException;
import br.com.marcio.filewatcher.exception.InvalidFormatException;
import br.com.marcio.filewatcher.output.ClientOutput;
import br.com.marcio.filewatcher.output.SaleOutput;
import br.com.marcio.filewatcher.output.SellerOutput;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest
public class FileServiceTest {

  private FileService fileService;
  private Path inputPath;
  private Path outputPath;

  @Before
  public void init() {

    inputPath = Paths.get("src/test/java/resources/data/in");
    outputPath = Paths.get("src/test/java/resources/data/out");

    // iniciando os objetos na mão para testar o construtor sem utilizar o spring
    ClientConverter clientConverter = new ClientConverter(new ClientBuilder());
    clientConverter.setRegex("(002)ç([0-9]{16})ç([\\s\\S]+)ç([\\s\\S]+)");

    SaleProductConverter saleProductConverter = new SaleProductConverter();
    saleProductConverter.setRegex("([0-9]+)-([0-9]+)-([0-9]*.?[0-9]+)");
    SaleConverter saleConverter = new SaleConverter(new SaleBuilder(saleProductConverter));
    saleConverter.setRegex("(003)ç([0-9]{2})ç(\\[.*\\]+)ç([\\s\\S]+)");

    SellerConverter sellerConverter = new SellerConverter(new SellerBuilder());
    sellerConverter.setRegex("(001)ç([0-9]{13})ç([\\s\\S]+)ç([0-9]*.?[0-9]+)");

    this.fileService = new FileService(clientConverter, sellerConverter, saleConverter);
  }

  @Test
  public void testGetValuesFromValidDatFile() throws IOException {

    Path testFilePath = inputPath.resolve("test.dat");
    Map<String, List<?>> result = this.fileService.getValuesFromDatFile(testFilePath);

    Assert.assertTrue(result.size() == 3);
  }

  @Test
  public void testInvalidFieldsDatFile() throws IOException {

    boolean status = true;
    try {
      Path testFilePath = inputPath.resolve("testInvalid.dat");
      this.fileService.getValuesFromDatFile(testFilePath);
    } catch (InvalidFieldException ex) {
      status = false;
    }
    Assert.assertFalse(status);
  }

  @Test
  public void testGetValuesFromInvalidDatFile() throws IOException {

    boolean status = true;
    try {
      Path testFilePath = inputPath.resolve("test.txt");
      this.fileService.getValuesFromDatFile(testFilePath);
    } catch (InvalidFormatException ex) {
      status = false;
    }
    Assert.assertFalse(status);
  }

  @Test
  public void testCreateFileFromResonseDatFile() throws IOException {

    Map<String, List<?>> response = new HashMap<>();

    ClientOutput testClient1 = createClient("test1", "test1", "test1");
    ClientOutput testClient2 = createClient("test2", "test2", "test2");

    response.put("clients", Arrays.asList(testClient1, testClient2));

    SellerOutput testSeller1 = createSeller("test1", "test1", 10.0);
    SellerOutput testSeller2 = createSeller("test2", "test2", 100.0);

    response.put("sellers", Arrays.asList(testSeller1, testSeller2));

    SaleProduct testProduct1 = createProduct(1, 10, 1000.00);
    SaleProduct testProduct2 = createProduct(2, 100, 10000.99);
    SaleProduct testProduct3 = createProduct(3, 1000, 10000.10);
    SaleProduct testProduct4 = createProduct(4, 10000, 10000);

    SaleOutput testSale1 =
        createSale(1, Arrays.asList(testProduct1, testProduct2), testSeller1.getName());
    SaleOutput testSale2 =
        createSale(2, Arrays.asList(testProduct3, testProduct4), testSeller2.getName());

    response.put("sales", Arrays.asList(testSale1, testSale2));

    Path path = this.fileService.createFileFromResponseDatFile(outputPath, response, "test");

    Assert.assertTrue(Files.isRegularFile(path));
    Files.delete(path);
    Assert.assertFalse(Files.isRegularFile(path));
  }

  @Test
  public void testCheckDatFilesInPath() throws IOException {
    List<Path> createdDoneFilesPath = fileService.checkDatFilesInPath(inputPath, outputPath);
    Assert.assertEquals(1, createdDoneFilesPath.size());
    for (Path doneFilePath : createdDoneFilesPath) {
      Assert.assertTrue(Files.isRegularFile(doneFilePath));
      Files.delete(doneFilePath);
      Assert.assertFalse(Files.isRegularFile(doneFilePath));
    }
  }

  private ClientOutput createClient(String name, String document, String businessArea) {
    return new ClientOutput(name, document, businessArea);
  }

  private SellerOutput createSeller(String name, String document, Double salary) {
    return new SellerOutput(name, document, salary);
  }

  private SaleOutput createSale(int id, List<SaleProduct> products, String sellerName) {
    return new SaleOutput(id, products, sellerName);
  }

  private SaleProduct createProduct(int id, int quantity, double price) {
    return new SaleProduct(id, quantity, price);
  }
}
